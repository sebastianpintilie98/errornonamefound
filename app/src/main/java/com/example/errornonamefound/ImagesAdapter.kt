package com.example.errornonamefound

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.errornonamefound.data.DataObject
import com.squareup.picasso.Picasso

class ImagesAdapter : RecyclerView.Adapter<ImagesAdapter.ImagesViewHolder>() {

    private val items = mutableListOf<DataObject>()

    class ImagesViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val image: ImageView = itemView.findViewById(R.id.instagramImage)
        val timestamp: TextView = itemView.findViewById(R.id.timestamp)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ImagesViewHolder =
        ImagesViewHolder(
            LayoutInflater.from(parent.context).inflate(R.layout.recyclerview_item, parent, false)
        )


    override fun onBindViewHolder(holder: ImagesViewHolder, position: Int) {
        holder.apply {
            val media = items[position]
            Picasso.get().load(media.media_url).into(image)
            timestamp.text = media.timestamp
        }
    }

    override fun getItemCount(): Int = items.size


    fun setItems(itemsList: List<DataObject>) {
        this.items.clear()
        this.items.addAll(itemsList)
    }

}