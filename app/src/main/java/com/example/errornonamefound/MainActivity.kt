package com.example.errornonamefound

import android.os.Bundle
import android.view.LayoutInflater
import androidx.appcompat.app.AppCompatActivity
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.errornonamefound.data.MediaResponse
import com.example.errornonamefound.data.UsernameObject
import com.example.errornonamefound.databinding.MainActivityBinding
import com.example.errornonamefound.utils.logD
import kotlinx.coroutines.runBlocking

class MainActivity : AppCompatActivity() {


    val mediaImagesAdapter by lazy { ImagesAdapter() }

    private lateinit var binding: MainActivityBinding

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = MainActivityBinding.inflate(layoutInflater)
        setContentView(binding.root)

        binding.imagesListRV.apply {
            adapter = mediaImagesAdapter
            layoutManager = LinearLayoutManager(this@MainActivity, RecyclerView.VERTICAL, false)
        }

        getInstagramData()
    }

    private fun getInstagramData(){

        runBlocking {
            val usernameResponse: UsernameObject = RetrofitClient.apiService.getUsername()

            val response: MediaResponse = RetrofitClient.apiService.getMediaInfo()
            response.data.forEach { data ->
                data.media_url
            }
            binding.username.text = usernameResponse.username
            mediaImagesAdapter.setItems(response.data)
            logD("api response: $response")
        }


    }

}