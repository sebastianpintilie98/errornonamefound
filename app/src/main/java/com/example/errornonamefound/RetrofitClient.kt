package com.example.errornonamefound

import com.example.errornonamefound.data.DataObject
import com.example.errornonamefound.data.MediaResponse
import com.example.errornonamefound.data.UsernameObject
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.*

object RetrofitClient {

    private val interceptor = HttpLoggingInterceptor().apply {
        setLevel(HttpLoggingInterceptor.Level.BODY)
    }

    private val client = OkHttpClient.Builder()
        .addInterceptor(interceptor)
        .build()

    private val retrofit = Retrofit.Builder().baseUrl(ApiService.BASE_URL)
        .client(client)
        .addConverterFactory(GsonConverterFactory.create())
        .build()

    val apiService: ApiService = retrofit.create(ApiService::class.java)

    interface ApiService {

        @GET("{user_id}/media?fields=timestamp,media_url,media_type")
        suspend fun getMediaInfo(
            @Path("user_id") userId: String = USER_ID,
            @Query("access_token") auth: String = AUTH
        ): MediaResponse

        @GET("{user_id}/?fields=username")
        suspend fun getUsername(
            @Path("user_id") userId: String = USER_ID,
            @Query("access_token") auth: String = AUTH
        ): UsernameObject

        companion object {
            const val AUTH =
                "IGQVJVaS1UbFdweGw0a3o0ekhHX3BxMmtNV2hGdE03bjB5X0ZAQM25YZAjlWdkpyYkc5TUdiRm5jY1dHUk85bngzMlNYTjlQT19WOXpUdWdscTBMYl9FLXJ4YUJoYV93VURDXzNxYlhB"
            const val USER_ID = "17841401347156899"
            const val BASE_URL = "https://graph.instagram.com/"
        }
    }
}