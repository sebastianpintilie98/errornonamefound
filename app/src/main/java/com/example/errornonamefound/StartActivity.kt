package com.example.errornonamefound

import android.os.Bundle
import android.content.Intent
import android.os.Handler
import android.view.WindowManager

import android.widget.TextView

import android.view.animation.AnimationUtils
import androidx.appcompat.app.AppCompatActivity
import butterknife.BindView
import butterknife.ButterKnife


class StartActivity : AppCompatActivity() {

    @BindView(R.id.error_text)
    lateinit var errorText: TextView

    @BindView(R.id.no_text)
    lateinit var noText: TextView

    @BindView(R.id.name_text)
    lateinit var nameText: TextView

    @BindView(R.id.found_text)
    lateinit var foundText: TextView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.splash_screen_activity)
        ButterKnife.bind(this)

        errorText.startAnimation(AnimationUtils.loadAnimation(this, R.anim.error_text_animation))
        noText.startAnimation(AnimationUtils.loadAnimation(this, R.anim.no_text_animation))
        nameText.startAnimation(AnimationUtils.loadAnimation(this, R.anim.name_text_animation))
        foundText.startAnimation(AnimationUtils.loadAnimation(this, R.anim.found_text_animation))

        window.setFlags(
            WindowManager.LayoutParams.FLAG_FULLSCREEN,
            WindowManager.LayoutParams.FLAG_FULLSCREEN
        )

        Handler().postDelayed({
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            finish()
        }, 4000)

    }
}