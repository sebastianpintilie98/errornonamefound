package com.example.errornonamefound.data


data class DataObject(
    val id: String = "",
    val timestamp: String = "",
    val media_url: String =  "",
    val media_type: String = ""
)