package com.example.errornonamefound.data

data class MediaResponse (
    val data: List<DataObject> = mutableListOf()
)