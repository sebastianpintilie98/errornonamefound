package com.example.errornonamefound.data

data class UsernameObject(
    val username: String = ""
)